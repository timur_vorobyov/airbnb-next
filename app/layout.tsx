import '@styles/globals.scss';
import { Nunito } from 'next/font/google';
import { Toaster } from 'react-hot-toast';
import Navbar from 'app/components/Navbar/Navbar';
import RegisterModal from './components/modals/RegisterModal/RegisterModal';
import LoginModal from './components/modals/LoginModal/LoginModal';
import getCurrentUser from './actions/getCurrentUser';
import RentModal from './components/modals/RentModal/RentModal';

const font = Nunito({ subsets: ['latin'] });

export const metadata = {
  title: 'Airbnb',
  description: 'Airbnb clone'
};

export default async function RootLayout({ children }: { children: React.ReactNode }) {
  const currentUser = await getCurrentUser();
  return (
    <html lang="en">
      <body className={font.className}>
        <Toaster />
        <RentModal />
        <RegisterModal />
        <LoginModal />
        <Navbar currentUser={currentUser} />
        <div className="box"> {children}</div>
      </body>
    </html>
  );
}
