import getCurrentUser from './actions/getCurrentUser';
import getListings, { IListingsParams } from './actions/getListings';
import Container from './components/Container/Container';
import EmptyState from './components/EmptyState/EmptyState';
import ListingCard from './components/listings/ListingCard/ListingCard';
import './Home.scss';

interface HomePageProps {
  searchParams: IListingsParams;
}
const Home = async ({ searchParams }: HomePageProps) => {
  const listings = await getListings(searchParams);
  const currentUser = await getCurrentUser();

  if (listings.length === 0) {
    return <EmptyState showReset />;
  }

  return (
    <Container>
      <div className="home">
        {listings.map((listing: any) => {
          return <ListingCard currentUser={currentUser} key={listing.id} data={listing} />;
        })}
      </div>
    </Container>
  );
};

export default Home;
export const dynamic = 'force-dynamic';
