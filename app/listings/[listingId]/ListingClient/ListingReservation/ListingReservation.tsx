'use client';

import { FC } from 'react';
import './ListingReservation.scss';
import { Range, RangeKeyDict } from 'react-date-range';
import Calendar from 'app/components/input/Calendar/Calendar';
import Button from 'app/components/Button/Button';

interface ListingReservationProps {
  price: number;
  totalPrice: number;
  onChangeDate: (value: Range) => void;
  dateRange: Range;
  onSubmit: () => void;
  disabled: boolean;
  disabledDates: Date[];
}
const ListingReservation: FC<ListingReservationProps> = ({
  price,
  totalPrice,
  onChangeDate,
  dateRange,
  onSubmit,
  disabled,
  disabledDates
}) => {
  return (
    <div className="listing-reservation">
      <div className="listing-reservation-price">
        <div className="price">$ {price}</div>
        <div className="night">night</div>
      </div>
      <hr />
      <Calendar
        value={dateRange}
        disabledDates={disabledDates}
        onChange={(value: RangeKeyDict) => {
          onChangeDate(value.selection);
        }}
      />
      <hr />
      <div style={{ padding: '16px' }}>
        <Button disabled={disabled} label="Reserve" onClick={onSubmit} />
      </div>
      <div className="total-price-wrapper">
        <div className="total-price-label">Total</div>
        <div className="total-price">$ {totalPrice}</div>
      </div>
    </div>
  );
};

export default ListingReservation;
