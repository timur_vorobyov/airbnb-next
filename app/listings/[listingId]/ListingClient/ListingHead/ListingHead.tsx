'use client';
import { FC } from 'react';
import './ListingHead.scss';
import { User } from '@prisma/client';
import Image from 'next/image';
import Heading from 'app/components/Heading/Heading';
import useCountries from 'app/hooks/useCountries';
import HeartButton from 'app/components/HeartButton/HeartButton';

interface ListingHeadPeops {
  title: string;
  locationValue: string;
  imageSrc: string;
  id: string;
  currentUser?: User | null;
}

const ListingHead: FC<ListingHeadPeops> = ({ title, locationValue, imageSrc, id, currentUser }) => {
  const { getByValue } = useCountries();
  const location = getByValue(locationValue);
  return (
    <>
      <Heading title={title} subtitle={`${location?.region}, ${location?.label}`} />
      <div className="listing-head">
        <Image alt="Image" src={imageSrc} fill className="image" />
        <div className="heart-btn">
          <HeartButton listingId={id} currentUser={currentUser} />
        </div>
      </div>
    </>
  );
};

export default ListingHead;
