'use client';
import { FC } from 'react';
import './ListingCategory.scss';
import { IconType } from 'react-icons/lib/cjs/iconBase';

interface ListingCategoryProps {
  icon: IconType;
  label: string;
  description: string;
}
const ListingCategory: FC<ListingCategoryProps> = ({ icon: Icon, label, description }) => {
  return (
    <div className="listing-category">
      <div className="listing-category-container">
        <Icon size={40} className="icon" />
        <div className="info">
          <div className="label">{label}</div>
          <div className="description">{description}</div>
        </div>
      </div>
    </div>
  );
};

export default ListingCategory;
