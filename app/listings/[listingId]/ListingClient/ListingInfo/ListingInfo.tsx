'use client';
import { FC } from 'react';
import './ListingInfo.scss';
import { User } from '@prisma/client';
import { IconType } from 'react-icons/lib/cjs/iconBase';
import dynamic from 'next/dynamic';
import useCountries from 'app/hooks/useCountries';
import Avatar from 'app/components/Avatar/Avatar';
import ListingCategory from './ListingCategory/ListingCategory';

const Map = dynamic(() => import('app/components/Map/Map'), {
  ssr: false
});

interface ListingInfoProps {
  user: User;
  description: string;
  guestCount: number;
  roomCount: number;
  bathroomCount: number;
  category:
    | {
        icon: IconType;
        label: string;
        description: string;
      }
    | undefined;
  locationValue: string;
}
const ListingInfo: FC<ListingInfoProps> = ({
  user,
  description,
  guestCount,
  roomCount,
  bathroomCount,
  category,
  locationValue
}) => {
  const { getByValue } = useCountries();
  const coordinates = getByValue(locationValue)?.latlng;

  return (
    <div className="listing-info">
      <div className="listing-info-container">
        <div> Hosted by {user?.name}</div>
        <Avatar />
      </div>
      <div className="counts">
        <div>{guestCount} guests</div>
        <div>{roomCount} rooms</div>
        <div>{bathroomCount} bathrooms</div>
      </div>
      <hr />
      {category && (
        <ListingCategory
          icon={category.icon}
          label={category.label}
          description={category.description}
        />
      )}
      <hr />
      <div className="listing-info-description">{description}</div>
      <hr />
      <Map center={coordinates} />
    </div>
  );
};

export default ListingInfo;
