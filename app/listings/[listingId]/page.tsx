import getListingById from 'app/actions/getListingById';
import EmptyState from 'app/components/EmptyState/EmptyState';
import getCurrentUser from 'app/actions/getCurrentUser';
import getReservations from 'app/actions/getReservations';
import ListingClient from './ListingClient/ListingClient';

interface IParams {
  listingId?: string;
}

const ListingPage = async ({ params }: { params: IParams }) => {
  const listing = await getListingById(params);
  const currentUser = await getCurrentUser();
  const reservations = await getReservations(params);

  if (!listing) {
    return <EmptyState showReset={false} />;
  }
  return (
    <div>
      {/* @ts-ignore */}
      <ListingClient listing={listing} currentUser={currentUser} reservations={reservations} />
    </div>
  );
};

export default ListingPage;
