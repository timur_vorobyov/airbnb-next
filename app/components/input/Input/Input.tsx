'use client';

import { FC } from 'react';
import './Input.scss';
import { BiDollar } from 'react-icons/bi';
import { FieldErrors, FieldValues, UseFormRegister } from 'react-hook-form/dist/types';

interface InputProps {
  id: string;
  label: string;
  type?: string;
  disabled?: boolean;
  formatPrice?: boolean;
  required?: boolean;
  register: UseFormRegister<FieldValues>;
  errors: FieldErrors;
}

const Input: FC<InputProps> = ({
  id,
  label,
  type = 'text',
  disabled,
  formatPrice,
  register,
  required,
  errors
}) => {
  return (
    <div className="input">
      {formatPrice && <BiDollar className="dollar-sign" size={24} />}
      <input
        className={`
        ${formatPrice && 'is-format-price'} 
        ${errors[id] && 'is-errors'} 
        `}
        id={id}
        disabled={disabled}
        type={type}
        {...register(id, { required })}
        placeholder=""
      />
      <label>{label}</label>
    </div>
  );
};

export default Input;
