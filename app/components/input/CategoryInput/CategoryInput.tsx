'use client';
import React, { FC } from 'react';
import './CategoryInput.scss';
import { IconType } from 'react-icons/lib/cjs/iconBase';

interface CategoryInputProps {
  icon: IconType;
  label: string;
  selected?: boolean;
  onClick: (value: string) => void;
}
const CategoryInput: FC<CategoryInputProps> = ({ icon: Icon, label, selected, onClick }) => {
  return (
    <div className={`category-input ${selected && 'selected'}`} onClick={() => onClick(label)}>
      <Icon size={30} />
      <div>{label}</div>
    </div>
  );
};

export default CategoryInput;
