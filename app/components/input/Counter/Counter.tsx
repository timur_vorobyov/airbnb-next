'use client';
import { FC, useCallback } from 'react';
import './Counter.scss';
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai';
interface CounterProps {
  title: string;
  subtitle: string;
  value: number;
  onChange: (value: number) => void;
}
const Counter: FC<CounterProps> = ({ title, subtitle, value, onChange }) => {
  const onAdd = useCallback(() => {
    onChange(value + 1);
  }, [onChange, value]);

  const onReduce = useCallback(() => {
    if (value === 1) return;

    onChange(value - 1);
  }, [onChange, value]);

  return (
    <div className="counter">
      <div className="counter-header__wrapper">
        <div className="counter-header__wrapper-title">{title}</div>
        <div className="counter-header__wrapper-subtitle">{subtitle}</div>
      </div>
      <div className="counter-controller">
        <div onClick={onReduce} className="counter-controller__item">
          <AiOutlineMinus />
        </div>
        <div className="counter-header__item-value">{value}</div>
        <div onClick={onAdd} className="counter-controller__item">
          <AiOutlinePlus />
        </div>
      </div>
    </div>
  );
};

export default Counter;
