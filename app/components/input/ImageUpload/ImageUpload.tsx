'use client';
import { FC, useCallback } from 'react';
import './ImageUpload.scss';
import { CldUploadWidget } from 'next-cloudinary';
import Image from 'next/image';
import { TbPhotoPlus } from 'react-icons/tb';

declare global {
  // eslint-disable-next-line no-var
  var cloudinary: any;
}

interface ImageUploadProps {
  onChange: (value: string) => void;
  value: string;
}

const ImageUpload: FC<ImageUploadProps> = ({ value, onChange }) => {
  const handleUpload = useCallback(
    (result: any) => {
      onChange(result.info.secure_url);
    },
    [onChange]
  );

  return (
    <CldUploadWidget onUpload={handleUpload} uploadPreset="zbvlgi9j" options={{ maxFiles: 1 }}>
      {({ open }) => {
        return (
          <div onClick={() => open?.()} className="image-upload">
            <TbPhotoPlus size={50} />
            <div className="image-upload__btn">Click to upload</div>
            {value && (
              <div className="image-upload__btn-img">
                <Image alt="Upload" fill style={{ objectFit: 'cover' }} src={value} />
              </div>
            )}
          </div>
        );
      }}
    </CldUploadWidget>
  );
};

export default ImageUpload;
