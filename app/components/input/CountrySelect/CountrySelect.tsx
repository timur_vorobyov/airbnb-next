'use client';

import { FC } from 'react';
import './CountrySelect.scss';
import Select from 'react-select';
import useCountries from 'app/hooks/useCountries';

export type CountrySelectValue = {
  flag: string;
  label: string;
  latlng: [number, number];
  region: string;
  value: string;
};

interface CountrySelectProps {
  value?: CountrySelectValue;
  onChange: (value: CountrySelectValue) => void;
}
const CountrySelect: FC<CountrySelectProps> = ({ value, onChange }) => {
  const { getAll } = useCountries();

  return (
    <div className="country-select">
      <Select
        placeholder="Anywhere"
        isClearable
        options={getAll()}
        value={value}
        onChange={(countryValue) => onChange(countryValue as CountrySelectValue)}
        formatOptionLabel={(option: any) => (
          <div className="options">
            <div className="flag">{option.flag}</div>
            <div className="label">
              {option.label}, <span>{option.region}</span>
            </div>
          </div>
        )}
        classNames={{
          control: () => 'control',
          input: () => 'input',
          option: () => 'option'
        }}
        theme={(theme) => ({
          ...theme,
          borderRadius: 6,
          colors: {
            ...theme.colors,
            primary: 'black',
            primary25: '#ffe4e6'
          }
        })}
      />
    </div>
  );
};

export default CountrySelect;
