'use client';

import { FC } from 'react';
import './Button.scss';
// eslint-disable-next-line import/named
import { IconType } from 'react-icons/lib/esm/iconBase';

interface ButtonProps {
  label: string;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  outline?: boolean;
  small?: boolean;
  icon?: IconType;
}

const Button: FC<ButtonProps> = ({ label, onClick, disabled, outline, small, icon: Icon }) => {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`button-component ${outline && 'outlined'} ${small && 'small'}`}>
      {Icon && <Icon size={24} className="icon-component" />}
      {label}
    </button>
  );
};

export default Button;
