'use client';

import { FC } from 'react';
import './Heading.scss';

interface HeadingProps {
  title: string;
  subtitle?: string;
  center?: boolean;
}
const Heading: FC<HeadingProps> = ({ title, subtitle, center }) => {
  return (
    <div className={`heading ${center && 'center'}`}>
      <div className="heading-title">{title}</div>
      <div className="heading-subtitle">{subtitle}</div>
    </div>
  );
};

export default Heading;
