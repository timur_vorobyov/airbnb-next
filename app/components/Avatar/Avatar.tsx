'use client';

import Image from 'next/image';
import './Avatar.scss';
import Placeholder from './assets/images/placeholder.jpg';

const Avatar = () => {
  return <Image className="avatar-image" height={30} width={30} alt="Avatar" src={Placeholder} />;
};

export default Avatar;
