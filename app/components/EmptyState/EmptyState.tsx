'use client';
import { FC } from 'react';
import './EmptyState.scss';
import { useRouter } from 'next/navigation';
import Heading from '../Heading/Heading';
import Button from '../Button/Button';

interface EmptyStateProps {
  title?: string;
  subtitle?: string;
  showReset: boolean;
}
const EmptyState: FC<EmptyStateProps> = ({
  title = 'No exact matches',
  subtitle = 'Try changing or removing some of your filters',
  showReset
}) => {
  const router = useRouter();
  return (
    <div className="empty-state">
      <Heading center title={title} subtitle={subtitle} />
      <div className="reset-btn">
        {showReset && (
          <Button outline label="Remove all filters" onClick={() => router.push('/')} />
        )}
      </div>
    </div>
  );
};

export default EmptyState;
