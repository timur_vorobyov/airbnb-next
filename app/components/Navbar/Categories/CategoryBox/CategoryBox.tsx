'use client';

import { FC, useCallback } from 'react';
import './CategoryBox.scss';
import { useRouter, useSearchParams } from 'next/navigation';
import qs from 'query-string';
import { IconType } from 'react-icons/lib/cjs/iconBase';

interface CategoryBoxProps {
  label: string;
  icon: IconType;
  selected?: boolean;
}
const CategoryBox: FC<CategoryBoxProps> = ({ icon: Icon, label, selected }) => {
  const router = useRouter();
  const params = useSearchParams();

  const handleClick = useCallback(() => {
    let currentQuery: { category?: string } = {};
    if (params) {
      currentQuery = qs.parse(params.toString());
    }

    currentQuery = {
      ...currentQuery,
      category: label
    };

    if (params?.get('category') === label) {
      delete currentQuery.category;
    }

    const url = qs.stringifyUrl(
      {
        url: '/',
        query: currentQuery
      },
      { skipNull: true }
    );

    router.push(url);
  }, [label, params, router]);

  return (
    <div onClick={handleClick} className={`category-box ${selected && 'selected'}`}>
      <Icon size={26} />
      <div className="category-box-label">{label}</div>
    </div>
  );
};

export default CategoryBox;
