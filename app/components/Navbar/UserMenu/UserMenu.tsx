'use client';

import { AiOutlineMenu } from 'react-icons/ai';
import { FC, useCallback, useState } from 'react';
import { User } from '@prisma/client';
import { signOut } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import Avatar from 'app/components/Avatar/Avatar';
import './UserMenu.scss';
import useRegisterModal from 'app/hooks/useRegisterModal';
import useLoginModal from 'app/hooks/useLoginModal';
import useRentModal from 'app/hooks/useRentModal';
import MenuItem from '../MenuItem/MenuItem';

interface UserMenuProps {
  currentUser?: User | null;
}

const UserMenu: FC<UserMenuProps> = ({ currentUser }) => {
  const router = useRouter();
  const [iseOpen, setIsOpen] = useState(false);
  const registerModal = useRegisterModal();
  const loginModal = useLoginModal();
  const rentModal = useRentModal();

  const toggleOpen = useCallback(() => {
    setIsOpen((value) => !value);
  }, []);

  const onRent = useCallback(() => {
    if (!currentUser) {
      return loginModal.onOpen();
    }

    rentModal.onOpen();
  }, [currentUser, loginModal, rentModal]);

  return (
    <div className="user-menu">
      <div className="user-menu-container">
        <div onClick={onRent} className="btn">
          Airbnb your home
        </div>
        <div onClick={toggleOpen} className="btn-icon-menu">
          <AiOutlineMenu />
          <div className="btn-icon-avatar">
            <Avatar />
          </div>
        </div>
      </div>
      {iseOpen && (
        <div className="toggle">
          <div className="toggle-container">
            {currentUser ? (
              <>
                <MenuItem onClick={() => router.push('/trips')} label="My trips" />
                <MenuItem onClick={() => router.push('/favorites')} label="My favorites" />
                <MenuItem onClick={() => router.push('/reservations')} label="My reservations" />
                <MenuItem onClick={() => router.push('/properties')} label="My properties" />
                <MenuItem onClick={rentModal.onOpen} label="Airbnb my home" />
                <hr />
                <MenuItem onClick={() => signOut()} label="Logout" />
              </>
            ) : (
              <>
                <MenuItem onClick={loginModal.onOpen} label="Login" />
                <MenuItem onClick={registerModal.onOpen} label="Sign up" />
              </>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default UserMenu;
