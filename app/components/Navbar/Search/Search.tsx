'use client';

import { BiSearch } from 'react-icons/bi';
import useSearchModal from 'app/hooks/useSearchModal';
import './Search.scss';

const Search = () => {
  const searchModal = useSearchModal();

  return (
    <div className="search" onClick={searchModal.onOpen}>
      <div className="search-container">
        <div className="search-place-btn">Anywhere</div>
        <div className="search-time-btn"> Any week</div>
        <div className="search-guest-btn-container">
          <div className="search-guest-btn">Add guests</div>
          <div className="search-guest-icon">
            <BiSearch size={18} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Search;
