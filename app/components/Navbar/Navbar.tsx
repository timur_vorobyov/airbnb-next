'use client';

import { FC } from 'react';
import { User } from '@prisma/client';
import Container from '../Container/Container';
import './Navbar.scss';
import Search from './Search/Search';
import UserMenu from './UserMenu/UserMenu';
import Logo from './assets/image-components/Logo/Logo';
import Categories from './Categories/Categories';

interface NavbarProps {
  currentUser?: User | null;
}

const Navbar: FC<NavbarProps> = ({ currentUser }) => {
  return (
    <div className="navbar">
      <div className="wrapper">
        <Container>
          <div className="row">
            <Logo />
            <Search />
            <UserMenu currentUser={currentUser} />
          </div>
        </Container>
      </div>
      <Categories />
    </div>
  );
};

export default Navbar;
