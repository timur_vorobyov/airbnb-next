'use client';

import { FC } from 'react';
import './MenuItem.scss';

interface MenuItemProps {
  onClick: () => void;
  label: string;
}

const MenuItem: FC<MenuItemProps> = ({ onClick, label }) => {
  return (
    <div onClick={onClick} className="menu-item">
      {label}
    </div>
  );
};

export default MenuItem;
