'use client';

import Image from 'next/image';
import { useRouter } from 'next/navigation';
import './Logo.scss';
import logo from '../../images/logo.png';

const Logo = () => {
  const router = useRouter();

  return (
    <Image onClick={() => router.push('/')} alt="logo" height="40" className="logo" src={logo} />
  );
};

export default Logo;
