'use client';

import { FC, useCallback, MouseEvent, useMemo } from 'react';
import './ListingCard.scss';
import { Listing, User } from '@prisma/client';
import { useRouter } from 'next/navigation';
import { format } from 'date-fns';
import Image from 'next/image';
import useCountries from 'app/hooks/useCountries';
import HeartButton from 'app/components/HeartButton/HeartButton';
import Button from 'app/components/Button/Button';
import { SafeReservation } from 'app/types';

interface ListingCardProps {
  data: Listing;
  reservation?: SafeReservation;
  onAction?: (id: string) => void;
  disabled?: boolean;
  actionLabel?: string;
  actionId?: string;
  currentUser?: User | null;
}
const ListingCard: FC<ListingCardProps> = ({
  data,
  reservation,
  onAction,
  disabled,
  actionLabel,
  actionId = '',
  currentUser
}) => {
  const router = useRouter();
  const { getByValue } = useCountries();

  const location = getByValue(data.locationValue);

  const handleCancel = useCallback(
    (e: MouseEvent<HTMLButtonElement>) => {
      e.stopPropagation();

      if (disabled) {
        return;
      }

      onAction?.(actionId);
    },
    [onAction, actionId, disabled]
  );

  const price = useMemo(() => {
    if (reservation) {
      return reservation.totalPrice;
    }

    return data.price;
  }, [reservation, data.price]);

  const reservationDate = useMemo(() => {
    if (!reservation) {
      return null;
    }

    const start = new Date(reservation.startDate);
    const end = new Date(reservation.endDate);

    return `${format(start, 'PP')} - ${format(end, 'PP')}`;
  }, [reservation]);

  return (
    <div onClick={() => router.push(`/listings/${data.id}`)} className="listing-card">
      <div className="listing-card__container">
        <Image fill alt="Listing" src={data.imageSrc} className="listing-card__container-img" />
        <div>
          <HeartButton listingId={data.id} currentUser={currentUser} />
        </div>
      </div>
      <div className="region">
        {location?.region}, {location?.label}
      </div>
      <div className="date">{reservationDate || data.category}</div>
      <div className="price">
        <div className="price-wrapper">$ {price}</div>
        {!reservation && <div className="night">night</div>}
      </div>
      {onAction && actionLabel && (
        <Button disabled={disabled} small label={actionLabel} onClick={handleCancel} />
      )}
    </div>
  );
};

export default ListingCard;
