'use client';
import { User } from '@prisma/client';
import { FC } from 'react';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import './HeartButton.scss';
import useFavorite from 'app/hooks/useFavorite';

interface HeartButtonProps {
  listingId: string;
  currentUser?: User | null;
}
const HeartButton: FC<HeartButtonProps> = ({ listingId, currentUser }) => {
  const { hasFavorited, toggleFavorite } = useFavorite({
    listingId,
    currentUser
  });

  return (
    <div onClick={toggleFavorite} className="heart-button">
      <AiOutlineHeart size={28} className="heart-icon-empty" />
      <AiFillHeart size={24} className={`heart-icon-filled ${hasFavorited && 'favorite'}`} />
    </div>
  );
};

export default HeartButton;
