import getCurrentUser from 'app/actions/getCurrentUser';
import EmptyState from 'app/components/EmptyState/EmptyState';
import getListings from 'app/actions/getListings';
import PropertiesClient from './PropertiesClient/PropertiesClient';

const PropertiesPage = async () => {
  const currentUser = await getCurrentUser();

  if (!currentUser) {
    return <EmptyState title="Unauthoruized" subtitle="Please login" showReset={false} />;
  }

  const listings = await getListings({
    userId: currentUser.id
  });

  if (Array.isArray(listings) && listings.length === 0) {
    return (
      <EmptyState
        title="No properties found"
        subtitle="Looks like you haven't reserved any properties"
        showReset={false}
      />
    );
  }

  return <PropertiesClient currentUser={currentUser} listings={listings} />;
};

export default PropertiesPage;
