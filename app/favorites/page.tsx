import React from 'react';
import EmptyState from 'app/components/EmptyState/EmptyState';
import getCurrentUser from 'app/actions/getCurrentUser';
import { getFavoriteListing } from 'app/actions/getFavoriteListing';
import FavoritesClient from './FavoritesClient/FavoritesClient';

const FavoritesPage = async () => {
  const currentUser = await getCurrentUser();
  const listings = await getFavoriteListing();

  if (listings.length === 0) {
    <EmptyState
      title="No favorites found"
      subtitle="Looks like you have no favorite listings."
      showReset={false}
    />;
  }

  if (!currentUser) {
    return <EmptyState title="Unauthoruized" subtitle="Please login" showReset={false} />;
  }

  return <FavoritesClient listings={listings} currentUser={currentUser} />;
};

export default FavoritesPage;
