import { FC } from 'react';
import './FavoritesClient.scss';
import { Listing, User } from '@prisma/client';
import Container from 'app/components/Container/Container';
import Heading from 'app/components/Heading/Heading';
import ListingCard from 'app/components/listings/ListingCard/ListingCard';

interface FavoritesClientProps {
  currentUser: User;
  listings: Listing[];
}

const FavoritesClient: FC<FavoritesClientProps> = ({ listings, currentUser }) => {
  return (
    <Container>
      <Heading title="Favorites" subtitle="List of places you have favorited!" />
      <div className="favorites-client">
        {listings.map((listing) => (
          <ListingCard key={listing.id} data={listing} currentUser={currentUser} />
        ))}
      </div>
    </Container>
  );
};

export default FavoritesClient;
