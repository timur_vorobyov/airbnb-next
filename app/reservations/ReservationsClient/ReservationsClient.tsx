'use client';

import { User } from '@prisma/client';
import { FC, useCallback, useState } from 'react';
import { useRouter } from 'next/navigation';
import toast from 'react-hot-toast';
import axios from 'axios';
import { SafeReservation } from 'app/types';
import Container from 'app/components/Container/Container';
import Heading from 'app/components/Heading/Heading';
import ListingCard from 'app/components/listings/ListingCard/ListingCard';
import './ReservationsClient.scss';

interface ReservationsClientProps {
  currentUser: User;
  reservations?: SafeReservation[];
}
const ReservationsClient: FC<ReservationsClientProps> = ({ currentUser, reservations }) => {
  const router = useRouter();
  const [deletingId, setDeletingId] = useState('');

  const onCancel = useCallback(
    (id: string) => {
      setDeletingId(id);

      axios
        .delete(`/api/reservations/${id}`)
        .then(() => {
          toast.success('Reservation canceled');
          router.refresh();
        })
        .catch((error) => {
          toast.error(error?.response?.data?.error);
        })
        .finally(() => {
          setDeletingId('');
        });
    },
    [router]
  );

  return (
    <Container>
      <Heading title="Reservation" subtitle="Booking on your properties" />
      <div className="reservations-client">
        {reservations?.map((reservation) => (
          <ListingCard
            key={reservation.id}
            data={reservation.listing}
            reservation={reservation}
            actionId={reservation.id}
            onAction={onCancel}
            disabled={deletingId === reservation.id}
            actionLabel="Cancel guest reservation"
            currentUser={currentUser}
          />
        ))}
      </div>
    </Container>
  );
};

export default ReservationsClient;
