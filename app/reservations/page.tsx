import getCurrentUser from 'app/actions/getCurrentUser';
import getReservations from 'app/actions/getReservations';
import EmptyState from 'app/components/EmptyState/EmptyState';
import ReservationsClient from './ReservationsClient/ReservationsClient';

const ReservationsPage = async () => {
  const currentUser = await getCurrentUser();

  if (!currentUser) {
    return <EmptyState title="Unauthoruized" subtitle="Please login" showReset={false} />;
  }

  const reservations = await getReservations({
    authorId: currentUser.id
  });

  if (reservations?.length === 0) {
    return (
      <EmptyState
        title="No reservations found"
        subtitle="Looks like you haven no reservations on your peoperties"
        showReset={false}
      />
    );
  }

  /* @ts-ignore */
  return <ReservationsClient currentUser={currentUser} reservations={reservations} />;
};

export default ReservationsPage;
