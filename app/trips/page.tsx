import getCurrentUser from 'app/actions/getCurrentUser';
import getReservations from 'app/actions/getReservations';
import EmptyState from 'app/components/EmptyState/EmptyState';
import TripsClient from './TripsClient/TripsClient';

const TripsPage = async () => {
  const currentUser = await getCurrentUser();

  if (!currentUser) {
    return <EmptyState title="Unauthoruized" subtitle="Please login" showReset={false} />;
  }

  const reservations = await getReservations({
    userId: currentUser.id
  });

  if (reservations?.length === 0) {
    return (
      <EmptyState
        title="No trips found"
        subtitle="Looks like you haven't reserved any trips"
        showReset={false}
      />
    );
  }

  /* @ts-ignore */
  return <TripsClient currentUser={currentUser} reservations={reservations} />;
};

export default TripsPage;
